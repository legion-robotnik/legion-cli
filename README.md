# Legion CLI

Commandline Utilities for the Legion Alerting and Monitoring System

## Setup & Usage

### Installation

```bash
pip install -U legion-cli --user
```

### Configuration

TODO

### Usage

The key reference for using `legion-cli` is:

```bash
legion-cli --help
```

## Development

### Standards

- Be excellent to each other
- Code coverage must be at 100% for all new code, or a good reason must be provided for why a given bit of code is not covered.
  - Example of an acceptable reason: "There is a bug in the code coverage tool and it says its missing this, but its not".
  - Example of unacceptable reason: "This is just exception handling, its too annoying to cover it".
- The code must pass the following analytics tools. Similar exceptions are allowable as in rule 2.
  - `pylint --disable=C0111,W1203,R0903 --max-line-length=100 ...`
  - `flake8 --max-line-length=100 ...`
  - `mypy --ignore-missing-imports --follow-imports=skip --strict-optional ...`
- All incoming information from users, clients, and configurations should be validated.
- All internal arguments passing should be typechecked whenever possible with `typeguard.typechecked`

### Development Setup

Using [poetry](https://python-poetry.org/) install from inside the repo directory:

```bash
pdm install
```

#### IDE Setup

**Sublime Text 3**

```bash
curl -sSL https://gitlab.com/-/snippets/2385805/raw/main/pdm.sublime-project.py | pdm run python > legion-cli.sublime-project
```

### Testing

All testing should be done with `pytest` which is installed with the `dev` requirements.

To run all the unit tests, execute the following from the repo directory:

```bash
pdm run pytest
```

This should produce a coverage report in `/path/to/legion-cli/htmlcov/`

While developing, you can use [`watchexec`](https://github.com/watchexec/watchexec) to monitor the file system for changes and re-run the tests:

```bash
watchexec -r -e py,yaml pdm run pytest
```

To run a specific test file:

```bash
pdm run pytest tests/unit/test_config.py
```

To run a specific test:

```bash
pdm run pytest tests/unit/test_config.py::test_config_loading
```

For more information on testing, see the `pytest.ini` file as well as the [documentation](https://docs.pytest.org/en/stable/).

#### Integration Testing

For integration testing, this code uses [testcontainers-rabbitmq](https://testcontainers-python.readthedocs.io/en/latest/rabbitmq/README.html). In order to enable this, you will need to install docker, and ensure that your user has the ability to interact with the docker service:

```bash
sudo apt install docker
sudo groupadd docker # may fail if the docker group already exists
sudo usermod -aG docker $USER
newgrp docker
docker run hello-world # verify that everything is working well
```

### Building & Publishing to PyPi

You can use [Twine](https://www.geeksforgeeks.org/how-to-publish-python-package-at-pypi-using-twine-module/) to publish this code to [PyPi](https://pypi.org/help/#basics) assuming you have an account and the relevant project permissions. This can be configured using a [`~/.pypirc` file]() like so:

```
[distutils]
  index-servers =
    pypi
    testpypi

[testpypi]
  username = __token__
  password = <PYPI TOKEN>

[pypi]
  username = __token__
  password = <PYPI TOKEN>
```

You can get the PyPi Tokens here: https://pypi.org/help/#apitoken

Once you have that set up, you can build, publish to the test server, and then the prod server with the following commands:

```bash
pdm build;

pdm run publish-test; # test

pdm run publish-prod; # prod
```